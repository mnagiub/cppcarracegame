/*
 * iphysix_engine.h
 *
 *  Created on: May 9, 2024
 *      Author: mnagiub
 */

#ifndef GAMEENGINE_IPHYSIX_ENGINE_H_
#define GAMEENGINE_IPHYSIX_ENGINE_H_

#include "iphysix_engine_types.h"

namespace gameengine {

class iPhysixEngine {
public:
	iPhysixEngine();
	virtual ~iPhysixEngine();
};

} /* namespace gameengine */

#endif /* GAMEENGINE_IPHYSIX_ENGINE_H_ */
