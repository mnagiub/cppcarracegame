/*
 * game_engine_types.h
 *
 *  Created on: May 8, 2024
 *      Author: mnagiub
 */

#ifndef GAMEENGINE_GAME_ENGINE_TYPES_H_
#define GAMEENGINE_GAME_ENGINE_TYPES_H_

#include "game_engine_includes.h"

namespace gameengine {

using namespace std;

enum class ErrorCode {
	E_OK = 0,
	E_INVARG = -1,
	E_INVPTR = -2,
	E_INVTYPE = -3,
	E_INVOP = -4,
	E_UNDEF = -5,
	E_BUSY = -6,
	E_UNAVAL = -7,
	E_DATAERR = -8,
	E_MATHINF = -9,
	E_MEMERR = -10
};

struct Pixel2D {
	int32_t x;
	int32_t y;
};

struct Vector2D {
	Pixel2D origin;
	double i;
	double j;
};

struct Line {
	Pixel2D p1;
	Pixel2D p2;
};

struct Rect2D {
	Pixel2D origin;
	Pixel2D center;
	int32_t width;
	int32_t height;
	double angle;
};

}	/* namespace gameengine */

#endif /* GAMEENGINE_GAME_ENGINE_TYPES_H_ */
