/*
 * igame_level.h
 *
 *  Created on: May 8, 2024
 *      Author: mnagiub
 */

#ifndef GAMEENGINE_IGAME_LEVEL_H_
#define GAMEENGINE_IGAME_LEVEL_H_

namespace gameengine {

class iGameLevel {
public:
	iGameLevel();
	virtual ~iGameLevel();
};

} /* namespace gameengine */

#endif /* GAMEENGINE_IGAME_LEVEL_H_ */
