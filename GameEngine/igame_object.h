/*
 * igame_object.h
 *
 *  Created on: May 9, 2024
 *      Author: mnagiub
 */

#ifndef GAMEENGINE_IGAME_OBJECT_H_
#define GAMEENGINE_IGAME_OBJECT_H_


#include "game_engine_includes.h"
#include "game_engine_types.h"
#include "iphysix_engine.h"
#include "igraphics_object.h"

namespace gameengine {

struct ScrObjPoint : public PhyObjVert2D, public iGraphicsObject {
private:
	Pixel2D pixel;

public:

	ScrObjPoint ()
		: PhyObjVert2D(), iGraphicsObject() {
		pixel.x = 0;
		pixel.y = 0;
	}

	ScrObjPoint (int32_t x1, int32_t y1)
		: PhyObjVert2D(x1, y1), iGraphicsObject() {
		pixel.x = 0;
		pixel.y = 0;
	}

	ScrObjPoint (const ScrObjPoint& other)
		: PhyObjVert2D(other.x, other.y), iGraphicsObject() {
		pixel.x = 0;
		pixel.y = 0;
		RenderGraphics();
	}

	ScrObjPoint& operator=(const ScrObjPoint& other)
		: PhyObjVert2D(), iGraphicsObject() {
		if (this != &other) {
			x = other.x;
			y = other.y;
			pixel.x = 0;
			pixel.y = 0;
			RenderGraphics();
		}
		return *this;
	}

	void RenderGraphics() override {
		pixel.x = x;
		pixel.y = -y;
	}

	Pixel2D GetScrPoint() {
		RenderGraphics();
		return pixel;
	}
};

struct ScrObjVect : public PhyObjVect2D, public iGraphicsObject {
private:
	Vector2D vect2d;

public:

	ScrObjVect()
		: PhyObjVect2D(), iGraphicsObject() {
		vect2d.origin.x = 0;
		vect2d.origin.y = 0;
		vect2d.i = 0;
		vect2d.j = 0;
	}

	ScrObjVect(const ScrObjVect& other)
		: PhyObjVect2D(other), iGraphicsObject() {
		vect2d.origin.x = 0;
		vect2d.origin.y = 0;
		vect2d.i = 0;
		vect2d.j = 0;
		RenderGraphics();
	}

	ScrObjVect& operator=(const ScrObjVect& other)
		: PhyObjVect2D(), iGraphicsObject() {
		if (this != &other) {
			origin.x = other.origin.x;
			origin.y = other.origin.y;
			i = other.i;
			j = other.j;
			angle = other.angle;
			vect2d.origin.x = 0;
			vect2d.origin.y = 0;
			vect2d.i = 0;
			vect2d.j = 0;
			RenderGraphics();
		}

		return *this;
	}

	void RenderGraphics() override {
		vect2d.origin.x = origin.x;
		vect2d.origin.y = -origin.y;
		vect2d.i = i;
		vect2d.j = -j;
	}

	Vector2D GetScrVector() {
		RenderGraphics();
		return vect2d;
	}
};

struct ScrObjLine : public PhyObjLine, public iGraphicsObject {
protected:
	Line line;
public:

	ScrObjLine()
		: PhyObjLine(), iGraphicsObject() {
		line.p1.x = 0;
		line.p1.y = 0;
		line.p2.x = 0;
		line.p2.y = 0;
	}

	ScrObjLine(const int32_t x1, const int32_t y1, const int32_t x2, const int32_t y2)
		: PhyObjLine(x1, y1, x2, y2), iGraphicsObject() {
		line.p1.x = x1;
		line.p1.y = y1;
		line.p2.x = x2;
		line.p2.y = y2;
		RenderGraphics();
	}

	ScrObjLine(const PhyObjVert2D op1, const PhyObjVert2D op2)
		: PhyObjLine(op1, op2), iGraphicsObject() {
		line.p1.x = p1.x;
		line.p1.y = p1.y;
		line.p2.x = p2.x;
		line.p2.y = p2.y;
		RenderGraphics();
	}

	ScrObjLine(const ScrObjLine& other)
		: PhyObjLine(other), iGraphicsObject() {
		line.p1.x = p1.x;
		line.p1.y = p1.y;
		line.p2.x = p2.x;
		line.p2.y = p2.y;
		RenderGraphics();
    }

    // Assignment operator
	ScrObjLine& operator=(const ScrObjLine& other)
		: PhyObjLine(other), iGraphicsObject() {
        if (this != &other) {
    		line.p1.x = p1.x;
    		line.p1.y = p1.y;
    		line.p2.x = p2.x;
    		line.p2.y = p2.y;
    		RenderGraphics();
        }
        return *this;
    }

	ErrorCode SetLinePoint(const int32_t x1, const int32_t y1, const int32_t x2, const int32_t y2) {
		PhyObjLine::UpdateCoordinates(x1, y1, x2, y2);
		line.p1.x = p1.x;
		line.p1.y = p1.y;
		line.p2.x = p2.x;
		line.p2.y = p2.y;
		RenderGraphics();
		return ErrorCode::E_OK;
	}

	ErrorCode SetLinePoint(const PhyObjVert2D op1, const PhyObjVert2D op2) {
		PhyObjLine::UpdateCoordinates(op1, op2);
		line.p1.x = p1.x;
		line.p1.y = p1.y;
		line.p2.x = p2.x;
		line.p2.y = p2.y;
		RenderGraphics();
		return ErrorCode::E_OK;
	}

	void RenderGraphics() override {
		line.p1.x = p1.x;
		line.p1.y = -p1.y;
		line.p2.x = p2.x;
		line.p2.y = -p2.y;
	}

	Line GetScrVector() {
		RenderGraphics();
		return line;
	}
};

struct PhyObjBoundBox {
private:
	int32_t x;
	int32_t y;
	int32_t width;
	int32_t height;

	bool IntersectionPoint(const PhyObjLine line, const PhyObjVect2D vect, PhyObjVert2D& point) {
	    double dx_line = line.p2.x - line.p1.x;
	    double dy_line = line.p2.y - line.p1.y;
	    double dx_vector = vect.i;
	    double dy_vector = vect.j;

	    double length = sqrt(dx_line * dx_line + dy_line * dy_line);
	    double dx_line_n = dx_line / length;
	    double dy_line_n = dy_line / length;

	    // Check if the line and vector are pointing in the same direction
	    double dot_product = dx_line_n * dx_vector + dy_line_n * dy_vector;
	    if (dot_product < 0) {
	    	return false;
	    }

	    // Check if the line and vector are parallel
	    double cross_product = dx_line * dy_vector - dx_vector * dy_line;
	    if (cross_product == 0) {
	    	return false;
	    }

	    // Solve for the parameter t in the line equation
	    double t = ((vect.origin.x - line.p1.x) * dy_vector - (vect.origin.y - line.p1.y) * dx_vector) / cross_product;

	    // Calculate the intersection point
	    point.x = line.p1.x + t * dx_line;
	    point.y = line.p1.y + t * dy_line;

	    return true;
	}

public:
	PhyObjBoundBox()
		: x(0), y(0), width(0), height(0) {
	}

	PhyObjBoundBox(const int32_t xx, const int32_t yy, const int32_t w, const int32_t h)
		: x(xx), y(yy), width(w), height(h) {
	}

	PhyObjBoundBox(const PhyObjBoundBox& other)
		: x(other.x), y(other.y), width(other.width), height(other.y) {
	}

	PhyObjBoundBox& operator=(const PhyObjBoundBox& other) {
		if (this != &other) {
			x = other.x;
			y = other.y;
			width = other.width;
			height = other.height;
		}

		return *this;
	}

	ErrorCode GenerateBoundingLines(PhyObjLine* &LinesList, uint8_t &len) {
		if (LinesList != nullptr) {
			return ErrorCode::E_INVARG;
		}

		LinesList = new PhyObjLine[4]();

		if (LinesList ==  nullptr) return ErrorCode::E_MEMERR;

		LinesList[0].UpdateCoordinates(x, y, x + width, y);
		LinesList[1].UpdateCoordinates(x, y, x, y - height);
		LinesList[2].UpdateCoordinates(x + width, y, x + width, y - height);
		LinesList[3].UpdateCoordinates(x, y - height, x + width, y - height);

		len = 4;

		return ErrorCode::E_OK;
	}

	bool CalculateCollisionPoint(const PhyObjVect2D vect, PhyObjVert2D &coll_point) {
		PhyObjLine* lines = nullptr;
		uint8_t len = 0;

		ErrorCode e = GenerateBoundingLines(lines, len);
		if (ErrorCode::E_OK != e) return false;

		bool r = false;
		for (int i = 0; i < len; i++) {
			IntersectionPoint(lines[i], vect, coll_point);
			PhyObjVect2D coll_vect = PhyObjVect2D::Normalize(coll_point.x, coll_point.y, vect.origin.x, vect.origin.y);
			double dotp = PhyObjVect2D::DotProduct(vect, coll_vect);
			if (dotp > 0) return true;
		}

		return r;
	}


#if 0
	// TODO: Fix the equation to detect collisions sides
	bool CalculateCollisionSides(const PhyObjVect2D vect, PhyObjLine* &LinesList, uint8_t &len) {
		PhyObjLine* lines = nullptr;
		uint8_t e_len = 0;
		uint8_t valid_cnt = 0;
		PhyObjVert2D point;
		bool* flags = nullptr;

		if (nullptr != LinesList) return false;

		ErrorCode e = GenerateBoundingLines(lines, e_len);
		if (ErrorCode::E_OK != e) return false;

		flags = new bool[e_len];
		if (nullptr == flags) return false;

		for (int i = 0; i < e_len; i++) flags[i] = false;

		for (int i = 0; i < e_len; i++) {
			if (IntersectionPoint(lines[i], vect, point)) {
				flags[i] = true;
				valid_cnt++;
			}
		}

		if (0 >= valid_cnt) return false;

		uint8_t cntr = 0;
		LinesList = new PhyObjLine[valid_cnt];
		for (int i = 0; i < e_len; i++) {
			if (flags[i]) {
				if (cntr < valid_cnt) {
					LinesList[cntr] = lines[i];
					cntr++;
				}
			}
		}

		len = valid_cnt;

		delete[] lines;
		delete[] flags;
		return true;
	}
#endif

	PhyObjVert2D GetItsCenter() {
		PhyObjVert2D center (0, 0);

		center.x = (double)x + ((double)width / (double)2);
		center.y = (double)y - ((double)height / (double)2);

		return center;
	}
};


class iGameObject {
protected:
	int32_t x;
	int32_t y;
	int32_t width;
	int32_t height;
	float ref_angle;

public:
	iGameObject() = delete;
	iGameObject(const int32_t x, const int32_t y, const int32_t w, const int32_t h, const float a);
	virtual ~iGameObject();


};

} /* namespace gameengine */

#endif /* GAMEENGINE_IGAME_OBJECT_H_ */
