/*
 * igraphics_object.h
 *
 *  Created on: May 9, 2024
 *      Author: mnagiub
 */

#ifndef GAMEENGINE_IGRAPHICS_OBJECT_H_
#define GAMEENGINE_IGRAPHICS_OBJECT_H_

#include "game_engine_includes.h"
#include "game_engine_types.h"

namespace gameengine {

class iGraphicsObject {
protected:
public:
	iGraphicsObject();
	virtual ~iGraphicsObject();
	virtual void RenderGraphics() = 0;
};

} /* namespace gameengine */

#endif /* GAMEENGINE_IGRAPHICS_OBJECT_H_ */
