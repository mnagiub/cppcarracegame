/*
 * igame_engine.h
 *
 *  Created on: May 8, 2024
 *      Author: mnagiub
 */

#ifndef GAMEENGINE_IGAME_ENGINE_H_
#define GAMEENGINE_IGAME_ENGINE_H_

#include "game_engine_includes.h"
#include "game_engine_types.h"

namespace gameengine {

using namespace std;

class iGameEngine {

protected:


public:
	iGameEngine();
	virtual ~iGameEngine();

	static iGameEngine* StartGameEngine(string assets_path);
	virtual ErrorCode InitializeGame() = 0;
	virtual ErrorCode StartGameLoop() = 0;
};

} /* namespace gameengine */

#endif /* GAMEENGINE_IGAME_ENGINE_H_ */
