/*
 * iphysix_engine_types.h
 *
 *  Created on: May 9, 2024
 *      Author: mnagiub
 */

#ifndef GAMEENGINE_IPHYSIX_ENGINE_TYPES_H_
#define GAMEENGINE_IPHYSIX_ENGINE_TYPES_H_

#include "game_engine_includes.h"
#include "game_engine_types.h"

namespace gameengine {

struct PhyObject {
protected:
	static inline Pixel2D RotatePixel(const Pixel2D pivot, double angle) {
	    angle = fmod(angle, 2.0 * M_PI);
	    if (angle < 0) {
	    	angle += 2.0 * M_PI;
	    }

	    double d_cos = (double)cos(angle);
	    double d_sin = (double)sin(angle);

	    if (angle > 0 && angle <= M_PI / 2.0) {
	    	d_cos = std::abs(d_cos);
	    	d_sin = std::abs(d_sin);
	    } else if (angle > M_PI / 2.0 && angle <= M_PI) {
	    	d_cos = -std::abs(d_cos);
	    	d_sin = std::abs(d_sin);
	    } else if (angle > M_PI && angle <= ((3.0 * M_PI) / 2.0)) {
	    	d_cos = -std::abs(d_cos);
	    	d_sin = -std::abs(d_sin);
	    } else if (angle > ((3.0 * M_PI) / 2.0) && angle <= (2.0 * M_PI)) {
	    	d_cos = std::abs(d_cos);
	    	d_sin = -std::abs(d_sin);
	    }

	    double x_new = pivot.x * d_cos - pivot.y * d_sin;
	    double y_new = pivot.x * d_sin + pivot.y * d_cos;

	    return {x_new, y_new};
	}

	static inline double CalculateAngleFromVector(const double dx, const double dy) {
		double angle = atan2(dy, dx);
		if (dy >= 0 && dx >= 0) {
			angle = std::abs(angle);
		} else if (dy >= 0 && dx < 0) {
			angle = std::abs(angle) + (M_PI / 2.0);
		} else if (dy < 0 && dx < 0) {
			angle = std::abs(angle) + M_PI;
		} else if (dy < 0 && dx >= 0) {
			angle = std::abs(angle) + (3.0 * M_PI / 2.0);
		}

		return angle;
	}

	static inline Pixel2D TranslatePixel(const Pixel2D p, const int32_t dx, const int32_t dy) {
		p.x += dx;
		p.y += dy;
	    return {p.x, p.y};
	}

	static inline bool CalculateCenter(const Pixel2D* const p_list, const size_t len, Pixel2D& pixel) {
		if (nullptr == p_list) return false;
		if (0 == len) return false;

		double cx = 0.0f, cy = 0.0f;

		for (int i = 0; i < len; i++) {
			cy += p_list[i].y;
		}

		cx /= len;
		cy /= len;
		pixel.x = cx;
		pixel.y = cy;

		return true;
	}

public:

	PhyObject() {

	}

	virtual ~PhyObject() {

	}

	virtual void Translate(const int32_t dx, const int32_t dy) {

	}

	virtual void Rotate(const double angle = 0, const Pixel2D* pivot = nullptr) {

	}
};

struct PhyObjVert2D : public PhyObject{
protected:
	Pixel2D pixel;
public:

	PhyObjVert2D ()
		: pixel{0, 0} {
	}

	PhyObjVert2D (int32_t x1, int32_t y1)
		: pixel{x1, y1} {
	}

	PhyObjVert2D (const PhyObjVert2D& other)
		: pixel{other.pixel.x, other.pixel.y} {
	}

	virtual ~PhyObjVert2D() {

	}

	PhyObjVert2D& operator=(const PhyObjVert2D& other) {
		if (this != &other) {
			pixel.x = other.pixel.x;
			pixel.y = other.pixel.y;
		}
		return *this;
	}

	virtual void Translate (const int32_t dx, const int32_t dy) override {
		pixel = TranslatePixel(pixel, dx, dy);
	}

	virtual void Rotate(const double angle = 0, const Pixel2D* pivot = nullptr) override {
		if (nullptr == pivot) return;

		pixel = RotatePixel(*pivot, angle);
	}

	virtual void UpdateCoordinates(const int32_t x, const int32_t y) {
		pixel.x = x;
		pixel.y = y;
	}

	virtual void UpdateCoordinates(const Pixel2D p) {
		pixel.x = p.x;
		pixel.y = p.y;
	}

	virtual Pixel2D GetPosition () const {
		return pixel;
	}
};

struct PhyObjVect2D : public PhyObject{
protected:
	Pixel2D origin;
	double i;
	double j;
	double angle;

public:
	PhyObjVect2D()
		: origin{0, 0}, i(0), j(0), angle(0) {
		Rotate(angle);
	}

	PhyObjVect2D(const PhyObjVect2D& other)
		: origin{other.origin.x, other.origin.y}, i(other.i), j(other.j), angle(other.angle) {
		Rotate(angle);
	}

	PhyObjVect2D& operator=(const PhyObjVect2D& other) {
		if (this != &other) {
			origin.x = other.origin.x;
			origin.y = other.origin.y;
			i = other.i;
			j = other.j;
			angle = other.angle;
		}

		return *this;
	}

	virtual ~PhyObjVect2D() {

	}

	virtual void Translate(const int32_t dx, const int32_t dy) override {
		origin = TranslatePixel(origin, dx, dy);
	}

	virtual void Rotate(const double new_angle = 0, const Pixel2D* const pivot = nullptr) override {
		if (nullptr == pivot) {
		    angle = fmod(new_angle, 2.0 * M_PI);
		    if (angle < 0) {
		    	angle += 2.0 * M_PI;
		    }

		    double d_cos = (double)cos(angle);
		    double d_sin = (double)sin(angle);

		    if (angle > 0 && angle <= M_PI / 2.0) {
		    	d_cos = std::abs(d_cos);
		    	d_sin = std::abs(d_sin);
		    } else if (angle > M_PI / 2.0 && angle <= M_PI) {
		    	d_cos = -std::abs(d_cos);
		    	d_sin = std::abs(d_sin);
		    } else if (angle > M_PI && angle <= ((3.0 * M_PI) / 2.0)) {
		    	d_cos = -std::abs(d_cos);
		    	d_sin = -std::abs(d_sin);
		    } else if (angle > ((3.0 * M_PI) / 2.0) && angle <= (2.0 * M_PI)) {
		    	d_cos = std::abs(d_cos);
		    	d_sin = -std::abs(d_sin);
		    }

		    double length = sqrt(d_cos * d_cos + d_sin * d_sin);

		    i = d_cos / (double)length;
		    j = d_sin / (double)length;
		} else {

		}
	}

	void UpdateCoordinates(Vector2D vect2d) {
		origin.x = vect2d.origin.x;
		origin.y = vect2d.origin.y;
		i = vect2d.i;
		j = vect2d.j;
		angle = CalculateAngleFromVector(i, j);
	}

	Vector2D GetCoordinates() const {
		return {{origin.x, origin.y}, i, j};
	}

	static PhyObjVect2D Normalize(const int32_t x1, const int32_t y1, const int32_t x2, const int32_t y2) {
		double dx = x1 - x2;
		double dy = y1 - y2;
		PhyObjVect2D vect;

		double len = sqrt(dx * dx + dy * dy);
		vect.i = dx / len;
		vect.j = dy / len;
		vect.angle = CalculateAngleFromVector(dx, dy);
		vect.origin.x = 0;
		vect.origin.y = 0;

		return vect;
	}

	static double DotProduct(const PhyObjVect2D vect1, const PhyObjVect2D vect2) {
		return (vect1.j * vect2.j) + (vect1.i * vect2.i);
	}

	static double CrossProduct(const PhyObjVect2D vect1, const PhyObjVect2D vect2) {
		return (vect1.i * vect2.j) - (vect1.j * vect2.i);
	}
};

struct PhyObjLine : public PhyObject {
protected:
	double slope;
	double intercept;
	Pixel2D p1;
	Pixel2D p2;

	bool is_between(double a, double b, double c) const {
		return (a <= b && b <= c) || (c <= b && b <= a);
	}

public:
	PhyObjLine()
		: slope(0), intercept(0), p1{0, 0}, p2{0, 0} {
	}

	PhyObjLine(const int32_t x1, const int32_t y1, const int32_t x2, const int32_t y2)
		: slope(0), intercept(0), p1{x1, y1}, p2{x2, y2} {
		CalculateSlope();
		CalculateIntercept();
	}

	PhyObjLine(const Pixel2D op1, const Pixel2D op2)
		: slope(0), intercept(0), p1{op1.x, op1.y}, p2{op2.x, op2.y} {
		CalculateSlope();
		CalculateIntercept();
	}

	PhyObjLine(const PhyObjVert2D op1, const PhyObjVert2D op2)
		: slope(0), intercept(0), p1{op1.pixel.x, op1.pixel.y}, p2{op2.pixel.x, op2.pixel.y} {
		CalculateSlope();
		CalculateIntercept();
	}

    PhyObjLine(const PhyObjLine& other)
    	: slope(other.slope), intercept(other.intercept), p1(other.p1), p2(other.p2) {
		CalculateSlope();
		CalculateIntercept();
    }

    // Assignment operator
    PhyObjLine& operator=(const PhyObjLine& other) {
        if (this != &other) {
            slope = other.slope;
            intercept = other.intercept;
            p1 = other.p1;
            p2 = other.p2;
    		CalculateSlope();
    		CalculateIntercept();
        }
        return *this;
    }

    virtual ~PhyObjLine() {

    }

	void UpdateCoordinates(const int32_t x1, const int32_t y1, const int32_t x2, const int32_t y2) {
		p1.x = x1;
		p2.x = x2;
		p1.y = y1;
		p2.y = y2;
		CalculateSlope();
		CalculateIntercept();
	}

	void UpdateCoordinates(const Pixel2D op1, const Pixel2D op2) {
		slope = 0;
		intercept = 0;
		p1.x = op1.x;
		p2.x = op2.x;
		p1.y = op1.y;
		p2.y = op2.y;
		CalculateSlope();
		CalculateIntercept();
	}

	void UpdateCoordinates(const PhyObjVert2D op1, const PhyObjVert2D op2) {
		slope = 0;
		intercept = 0;
		p1.x = op1.pixel.x;
		p2.x = op2.pixel.x;
		p1.y = op1.pixel.y;
		p2.y = op2.pixel.y;
		CalculateSlope();
		CalculateIntercept();
	}

	Line GetCoorindates() {
		return {{p1.x, p1.y},{p2.x, p2.y}};
	}

	virtual void Translate(const int32_t dx, const int32_t dy) override {
		p1.x += dx;
		p1.y += dy;
		p2.x += dx;
		p2.y += dy;
	}

	virtual void Rotate(const double new_angle = 0, const Pixel2D* const pivot = nullptr) override {
		Pixel2D temp_p1 = {p1.x, p1.y};
		Pixel2D temp_p2 = {p2.x, p2.y};

		if (nullptr == pivot) {
			Pixel2D midpoint = {(p1.x + p2.x) / 2, (p1.y + p2.y) / 2};

			temp_p1.x -= midpoint.x;
			temp_p1.y -= midpoint.y;
			temp_p2.x -= midpoint.x;
			temp_p2.y -= midpoint.y;

		    Pixel2D start = RotatePixel(temp_p1, new_angle);
		    Pixel2D end = RotatePixel(temp_p2, new_angle);

		    p1.x = temp_p1.x + midpoint.x;
		    p1.y = temp_p1.y + midpoint.y;
		    p2.x = temp_p2.x + midpoint.x;
		    p2.y = temp_p2.y + midpoint.y;
		} else {
			temp_p1.x -= pivot->x;
			temp_p1.y -= pivot->y;
			temp_p2.x -= pivot->x;
			temp_p2.y -= pivot->y;

		    Pixel2D start = RotatePixel(temp_p1, new_angle);
		    Pixel2D end = RotatePixel(temp_p2, new_angle);

		    p1.x = temp_p1.x + pivot->x;
		    p1.y = temp_p1.y + pivot->y;
		    p2.x = temp_p2.x + pivot->x;
		    p2.y = temp_p2.y + pivot->y;
		}
	}

	double CalculateSlope() {
		if (p1.x == p2.x) {
			slope = std::numeric_limits<double>::infinity();
		} else {
			slope = (p2.y - p1.y) / (p2.x - p1.x);
		}
		return slope;
	}

	double GetSlope() const {
		return slope;
	}

	double CalculateIntercept() {
		intercept = p1.y - CalculateSlope() * p1.x;
		return intercept;
	}

	double GetIntercept()const {
		return intercept;
	}

	bool CheckIntersection(PhyObjLine otherLine) {
	    double m1 = CalculateSlope();
	    double m2 = otherLine.CalculateSlope();

	    // If the slopes are equal, the lines are parallel
	    if (m1 == m2) {
	        return false;
	    }

	    double b1 = CalculateIntercept();
	    double b2 = otherLine.CalculateIntercept();

	    double x_intersect = (b2 - b1) / (m1 - m2);
	    double y_intersect = m1 * x_intersect + b1;

	    // Check if the intersection point is within the line segments
	    return is_between(p1.x, x_intersect, p2.x) && is_between(p1.y, y_intersect, p2.y) &&
	    		is_between(otherLine.p1.x, x_intersect, otherLine.p2.x) &&
				is_between(otherLine.p1.y, y_intersect, otherLine.p2.y);
	}
};

struct PhyObjBoundBox : public PhyObject{
protected:
	Pixel2D origin;
	Pixel2D p2;
	Pixel2D p3;
	Pixel2D p4;

	bool IntersectionPoint(const Line line, const Vector2D vect, PhyObjVert2D& point) {
	    double dx_line = line.p2.x - line.p1.x;
	    double dy_line = line.p2.y - line.p1.y;
	    double dx_vector = vect.i;
	    double dy_vector = vect.j;

	    double length = sqrt(dx_line * dx_line + dy_line * dy_line);
	    double dx_line_n = dx_line / length;
	    double dy_line_n = dy_line / length;

	    // Check if the line and vector are pointing in the same direction
	    double dot_product = dx_line_n * dx_vector + dy_line_n * dy_vector;
	    if (dot_product < 0) {
	    	return false;
	    }

	    // Check if the line and vector are parallel
	    double cross_product = dx_line * dy_vector - dx_vector * dy_line;
	    if (cross_product == 0) {
	    	return false;
	    }

	    // Solve for the parameter t in the line equation
	    double t = ((vect.origin.x - line.p1.x) * dy_vector - (vect.origin.y - line.p1.y) * dx_vector) / cross_product;

	    // Calculate the intersection point
	    point.UpdateCoordinates(line.p1.x + t * dx_line, line.p1.y + t * dy_line);

	    return true;
	}

public:
	PhyObjBoundBox()
		: origin{0, 0}, p2{0, 0}, p3{0, 0}, p4{0, 0} {
	}

	PhyObjBoundBox(const Pixel2D org, const Pixel2D p_2, Pixel2D p_3, Pixel2D p_4)
		: origin{org.x, org.y}, p2{p_2.x, p_2.y}, p3{p_3.x, p_3.y}, p4{p_4.x, p_4.y} {
	}

	PhyObjBoundBox(const PhyObjBoundBox& other)
		: origin{other.origin.x, other.origin.y},
		  p2{other.p2.x, other.p2.y},
		  p3{other.p3.x, other.p3.y},
		  p4{other.p4.x, other.p4.y} {
	}

	PhyObjBoundBox& operator=(const PhyObjBoundBox& other) {
		if (this != &other) {
			origin = {other.origin.x, other.origin.y};
			p2 = {other.p2.x, other.p2.y};
			p3 = {other.p3.x, other.p3.y};
			p4 = {other.p4.x, other.p4.y};
		}
		return *this;
	}

	virtual ~PhyObjBoundBox() {

	}

	void UpdateCoordinates (const Pixel2D org, const Pixel2D p_2, Pixel2D p_3, Pixel2D p_4) {
		origin = {org.x, org.y};
		p2 = {p_2.x, p_2.y};
		p3 = {p_3.x, p_3.y};
		p4 = {p_4.x, p_4.y};
	}

	Rect2D GetCoordinates() const {
		Rect2D rect;
		rect.origin = {origin.x, origin.y};
		rect.center = {0, 0};
		rect.height = 0;
		rect.width = 0;
		rect.angle = 0.0f;

		double d_x = p2.x - origin.x;
		double d_y = p2.y - origin.y;

		rect.width = sqrt(d_x * d_x + d_y * d_y);

		d_x = origin.x - p4.x;
		d_y = origin.y - p4.y;

		rect.height = sqrt(d_x * d_x + d_y * d_y);

		d_x /= rect.height;
		d_y /= rect.height;

		rect.angle = CalculateAngleFromVector(d_x, d_y);
		Pixel2D p_list[] = {{origin.x, origin.y}, {p2.x, p2.y}, {p3.x, p3.y}, {p4.x, p4.y}};
		CalculateCenter(p_list, 4, rect.center);

		return rect;
	}

	virtual void Translate(const int32_t dx, const int32_t dy) override {
		origin.x += dx;
		origin.y += dy;
		p2.x += dx;
		p2.y += dy;
		p3.x += dx;
		p3.y += dy;
		p4.x += dx;
		p4.y += dy;
	}

	virtual void Rotate(const double new_angle = 0, const Pixel2D* pivot = nullptr) override {
		Pixel2D p_list[] = {{origin.x, origin.y}, {p2.x, p2.y}, {p3.x, p3.y}, {p4.x, p4.y}};

		Pixel2D new_pivot;

		if (nullptr == pivot) {
			CalculateCenter(p_list, 4, new_pivot);
		} else {
			new_pivot.x = pivot->x;
			new_pivot.y = pivot->y;
		}

		for (int i = 0; i < 4; i++) {
			p_list[i].x -= new_pivot.x;
			p_list[i].y -= new_pivot.y;
		}

		for (int i = 0; i < 4; i++) {
			p_list[i] = RotatePixel(p_list[i], new_angle);
		}

		origin.x = p_list[0].x + new_pivot.x;
		origin.y = p_list[0].y + new_pivot.y;
		p2.x = p_list[1].x + new_pivot.x;
		p2.y = p_list[1].y + new_pivot.y;
		p3.x = p_list[2].x + new_pivot.x;
		p3.y = p_list[2].y + new_pivot.y;
		p4.x = p_list[3].x + new_pivot.x;
		p4.y = p_list[3].y + new_pivot.y;
	}

	bool GenerateBoundingLines(PhyObjLine* &LinesList, size_t &len) {
		if (LinesList != nullptr) {
			return false;
		}

		LinesList = new PhyObjLine[4]();

		if (LinesList ==  nullptr) return false;

		LinesList[0].UpdateCoordinates((Pixel2D){origin.x, origin.y}, (Pixel2D){p2.x, p2.y});
		LinesList[1].UpdateCoordinates((Pixel2D){p2.x, p2.y}, (Pixel2D){p3.x, p3.y});
		LinesList[2].UpdateCoordinates((Pixel2D){p3.x, p3.y}, (Pixel2D){p4.x, p4.y});
		LinesList[3].UpdateCoordinates((Pixel2D){origin.x, origin.y}, (Pixel2D){p4.x, p4.y});

		len = 4;

		return true;
	}

	bool CalculateCollisionPoint(const PhyObjVect2D vect, PhyObjVert2D &coll_point) {
		PhyObjLine* lines = nullptr;
		uint8_t len = 0;

		bool e = GenerateBoundingLines((PhyObjLine* &)lines, (size_t&)len);
		if (false == e) return false;

		bool r = false;
		for (int i = 0; i < len; i++) {
			IntersectionPoint(lines[i].GetCoorindates(), vect.GetCoordinates(), coll_point);
			PhyObjVect2D coll_vect = PhyObjVect2D::Normalize(coll_point.GetPosition().x, coll_point.GetPosition().y, vect.origin.x, vect.origin.y);
			double dotp = PhyObjVect2D::DotProduct(vect, coll_vect);
			if (dotp > 0) return true;
		}

		return r;
	}


#if 0
	// TODO: Fix the equation to detect collisions sides
	bool CalculateCollisionSides(const PhyObjVect2D vect, PhyObjLine* &LinesList, uint8_t &len) {
		PhyObjLine* lines = nullptr;
		uint8_t e_len = 0;
		uint8_t valid_cnt = 0;
		PhyObjVert2D point;
		bool* flags = nullptr;

		if (nullptr != LinesList) return false;

		bool e = ((PhyObjLine* &)lines, (size_t&)len);
		if (true != e) return false;

		flags = new bool[e_len];
		if (nullptr == flags) return false;

		for (int i = 0; i < e_len; i++) flags[i] = false;

		for (int i = 0; i < e_len; i++) {
			if (IntersectionPoint(lines[i].GetCoorindates(), vect.GetCoordinates(), point)) {
				flags[i] = true;
				valid_cnt++;
			}
		}

		if (0 >= valid_cnt) return false;

		uint8_t cntr = 0;
		LinesList = new PhyObjLine[valid_cnt];
		for (int i = 0; i < e_len; i++) {
			if (flags[i]) {
				if (cntr < valid_cnt) {
					LinesList[cntr] = lines[i];
					cntr++;
				}
			}
		}

		len = valid_cnt;

		delete[] lines;
		delete[] flags;
		return true;
	}
#endif

	PhyObjVert2D GetItsCenter() const {
		Pixel2D p_list[] = {{origin.x, origin.y}, {p2.x, p2.y}, {p3.x, p3.y}, {p4.x, p4.y}};

		Pixel2D center;

		CalculateCenter(p_list, 4, center);

		return center;
	}
};

}



#endif /* GAMEENGINE_IPHYSIX_ENGINE_TYPES_H_ */
