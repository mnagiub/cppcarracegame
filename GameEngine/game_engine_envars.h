/*
 * game_engine_envars.h
 *
 *  Created on: May 8, 2024
 *      Author: mnagiub
 */

#ifndef GAMEENGINE_GAME_ENGINE_ENVARS_H_
#define GAMEENGINE_GAME_ENGINE_ENVARS_H_

#include "game_engine_includes.h"
#include "game_engine_types.h"

namespace gameengine {

namespace {
	using namespace std;
	const string kAssetsFolderPath = ".\\assets\\";
	uint32_t screen_width = 0;
	uint32_t screen_height = 0;
}

}	/* namespace gameengine */

#endif /* GAMEENGINE_GAME_ENGINE_ENVARS_H_ */
