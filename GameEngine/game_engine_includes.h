/*
 * game_engine_includes.h
 *
 *  Created on: May 8, 2024
 *      Author: mnagiub
 */

#ifndef GAMEENGINE_GAME_ENGINE_INCLUDES_H_
#define GAMEENGINE_GAME_ENGINE_INCLUDES_H_

namespace gameengine {


#define SDL_MAIN_HANDLED

#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <cmath>
#include <cstdint>
#include <limits>

}	/* namespace gameengine */

#endif /* GAMEENGINE_GAME_ENGINE_INCLUDES_H_ */
