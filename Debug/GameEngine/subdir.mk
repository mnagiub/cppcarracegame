################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../GameEngine/igame_engine.cpp \
../GameEngine/igame_level.cpp \
../GameEngine/igame_object.cpp \
../GameEngine/iphysix_engine.cpp 

CPP_DEPS += \
./GameEngine/igame_engine.d \
./GameEngine/igame_level.d \
./GameEngine/igame_object.d \
./GameEngine/iphysix_engine.d 

OBJS += \
./GameEngine/igame_engine.o \
./GameEngine/igame_level.o \
./GameEngine/igame_object.o \
./GameEngine/iphysix_engine.o 


# Each subdirectory must supply rules for building sources it contributes
GameEngine/%.o: ../GameEngine/%.cpp GameEngine/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -I"C:\opt\sdl2-devel-2.30.3-mingw\SDL2-2.30.3\i686-w64-mingw32\include\SDL2" -I"C:\opt\sdl2_image-2.8.2\i686-w64-mingw32\include\SDL2" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-GameEngine

clean-GameEngine:
	-$(RM) ./GameEngine/igame_engine.d ./GameEngine/igame_engine.o ./GameEngine/igame_level.d ./GameEngine/igame_level.o ./GameEngine/igame_object.d ./GameEngine/igame_object.o ./GameEngine/iphysix_engine.d ./GameEngine/iphysix_engine.o

.PHONY: clean-GameEngine

